from rest_framework import serializers

from .models import VideoUrl

class VideoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VideoUrl
        fields = ('url')

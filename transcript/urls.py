from django.urls import path

from .views import SnippetList

urlpatterns = [
    # path('', views.index, name='index'),
    path('api', SnippetList.as_view()),
]

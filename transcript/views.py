import random
import string
import time
import pickle
import azure.cognitiveservices.speech as speechsdk
from django.shortcuts import render
from django.http import HttpResponse
from moviepy.editor import VideoFileClip
from moviepy.video.io import ffmpeg_tools
from .serializers import VideoSerializer
from .models import VideoUrl
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# Flag to end transcription
done = False
# List of transcribed lines
results = list()
url = ''


def getRandomText():
    char_set = string.ascii_uppercase + string.digits
    random_text = ''.join(random.sample(char_set * 6, 6))
    return random_text


def addFile():
    try:
        filePath = "audio/audio_" + getRandomText() + ".wav"
        ffmpeg_tools.ffmpeg_extract_audio(url, filePath)
        return filePath
    except:
        return False


def getspeechRecognizer():
    file_name = addFile()
    if file_name:
        subscription_key = " 06a79298e24448b39ccca9d5d1ad8535"
        speech_region = "eastus"
        # Authenticate
        speech_config = speechsdk.SpeechConfig(subscription_key, speech_region)
        # Set up the file as the audio source
        audio_config = speechsdk.AudioConfig(filename=file_name)
        speech_recognizer = speechsdk.SpeechRecognizer(speech_config, audio_config)

        return speech_recognizer
    return file_name


def callScript():
    speech_recognizer = getspeechRecognizer()
    if speech_recognizer:
        # Define behaviour for each recognition/transcription
        speech_recognizer.recognized.connect(recognised)

        # Define behaviour for end of session
        speech_recognizer.session_stopped.connect(stop_cb)
        # And for canceled sessions
        speech_recognizer.canceled.connect(stop_cb)

        # Create a synchronous continuous recognition, the transcription itself if you will
        speech_recognizer.start_continuous_recognition()
        while not done:
            time.sleep(0.5)
            if done:
                return listToString(results)
    return speech_recognizer
    # transcribed_video = "transcribed/transcribed_video_" + getRandomText() + ".pickle"
    # # Dump the whole transcription to a pickle file
    # with open(transcribed_video, "wb") as f:
    #     pickle.dump(results, f)
    #     print("Transcription dumped")


def stop_cb(evt):
    """callback that stops continuous recognition upon receiving an event `evt`"""
    # print(f"CLOSING on {evt}")
    speech_recognizer = getspeechRecognizer()
    speech_recognizer.stop_continuous_recognition()
    # Let the function modify the flag defined outside this function
    global done
    done = True
    # print(f"CLOSED on {evt}")


def recognised(evt):
    """Callback to process a single transcription"""
    recognised_text = evt.result.text
    # Simply append the new transcription to the running list
    results.append(recognised_text)
    print(f"Audio transcription: '{recognised_text}'")


def listToString(s):
    # initialize an empty string
    str1 = " "
    # return string
    return (str1.join(s))


class SnippetList(APIView):
    def post(self, request, format=None):
        global url
        url = request.data['url']
        call_script = callScript()
        # serializer.save()
        if call_script:
            print(call_script)
            content = {
                "status": "success",
                "transcript": call_script
            }
            return Response(content, status=status.HTTP_201_CREATED)
        content = {
            "status": "failure"
        }
        return Response(content, status=status.HTTP_400_BAD_REQUEST)
